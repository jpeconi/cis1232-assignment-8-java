
import java.util.ArrayList;
import utility.Utility;

/**
 *
 * @author Ryan
 * @since March 23, 2014
 *
 * This class is used for counting the rooms as well as searching for a room
 * that suits the users need.
 */
public class RoomUtility {

    public static void roomCount(ArrayList<Room> rooms) {
        int numOfRoom = 0;
        int numOfComputerRoom = 0;
        int numOfBoardRoom = 0;
        int numOfBoardRoomPro = 0;
        int numOfBiologyRoom = 0;
        int largestRoom = 0;
        int largestNumOfRooms = 0;
        for (int i = 0; i < rooms.size(); i++) {

            System.out.println("This room is a: " + rooms.get(i).getClass().toString());
            if (rooms.get(i).getClass().toString().equals("class Room")) {
                numOfRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class ComputerRoom")) {
                numOfComputerRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BoardRoom")) {
                numOfBoardRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BiologyLab")) {
                numOfBiologyRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BoardRoomPro")) {
                numOfBoardRoomPro += 1;
            }

            if (rooms.get(i).getNumberOfSeats() > largestNumOfRooms) {//Determine largest room
                largestNumOfRooms = rooms.get(i).getNumberOfSeats();
                largestRoom = rooms.get(i).getRoomNumber();
            }
        }
        //Display the results of counting 
        System.out.println("Room Count Details Report"
                + "\nRooms: " + numOfRoom
                + "\nComputer Rooms: " + numOfComputerRoom
                + "\nBiology Labs: " + numOfBiologyRoom
                + "\nBoard Rooms: " + numOfBoardRoom
                + "\nFancy Board Rooms: " + numOfBoardRoomPro
                + "\n\nLargest Room is #" + largestRoom + " with " + largestNumOfRooms + "seats");
    }

    /**
     * @author Jamison Peconi
     * @since 04/18/2016
     * @param rooms
     *
     * This method searches for a room. It contains the functionality to verify
     * the user input using exception handling. I updated this method for the
     * assignment. This method makes a call to the utility class and passes a
     * range to ensure that the user enters data within the correct range.
     */
    public static void roomSearch(ArrayList<Room> rooms) {
        int roomTypeNum;
        // Use the overloadede validate method that contains a range feature
        roomTypeNum = Utility.validateNumberInput("1) Room\n"
                + "2) Computer Lab\n"
                + "3) Board Room\n"
                + "4) Biology lab\n"
                + "5) Fancy Board Room", 1, 5);
        String roomTypeString = "";

        if (roomTypeNum == 1) {
            roomTypeString = "class Room";
        }
        if (roomTypeNum == 2) {
            roomTypeString = "class ComputerRoom";
        }
        if (roomTypeNum == 3) {
            roomTypeString = "class BoardRoom";
        }
        if (roomTypeNum == 4) {
            roomTypeString = "class BiologyLab";
        }
        if (roomTypeNum == 5) {
            roomTypeString = "class BoardRoomPro";
        }

        // Validate the user input for number of seats
        int numOfSeats = Utility.validateNumberInput("How many seats do you need?");
        for (int i = 0; i < rooms.size(); i++) {
            if (rooms.get(i).getClass().toString().equals(roomTypeString)) {//If it is the right type of room..
                if (!rooms.get(i).isReserved()) {                           //<--and the room is not reserved.. 
                    if (rooms.get(i).getNumberOfSeats() >= numOfSeats) {    //<-- and it also has the right number of seats or more.
                        System.out.println("\n\n******************************"
                                + "\nRoom Number: " + rooms.get(i).getRoomNumber()
                                + "\nNumber of Seats: " + rooms.get(i).getNumberOfSeats()
                                + "\nSmart Board: " + rooms.get(i).isHasSmartBoard());
                    }
                }
            }

        }

    }

}
