
import exception.RoomExistException;
import exception.RoomReservationException;
import java.util.ArrayList;
import java.util.Scanner;
import utility.Utility;

/**
 * This main method will invoke functionality for booking college rooms.
 *
 * Detailed description:
 * https://docs.google.com/document/d/1jyrvSJHXS6BZuXKVswYkmt2muBmPI71OxXTLQxerDVU/edit
 *
 * @author cis1232 (including Roger)
 *
 * UPDATED################## This class has been modified by Ryan Forrester to
 * work with the new classes that have been added.
 */
public class RoomMain {

    private static final int ROOM_DOES_NOT_EXIST = -1;
    private static ArrayList<Room> rooms = new ArrayList();
    private static int roomNum;

    /**
     * Main method controls program and user interface.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String menu = "Choose an option:\n"
                + "1) Add Room\n"
                + "2) Reserve Room\n"
                + "3) Release Room\n"
                + "4) Show Rooms\n"
                + "5) Room Count\n"
                + "6) Room Search\n"
                + "7) Exit";
        int selection = 0;

        while (selection != 7) {
            selection = Utility.validateNumberInput(menu);
            switch (selection) {
                case 1:
                    addRoom();
                    break;
                case 2:
                    try {
                        reserveRoom();
                    } catch (RoomExistException | RoomReservationException e) {
                        System.out.println(e);
                    }
                    break;
                case 3:
                    try {
                        releaseRoom();
                    } catch (RoomExistException | RoomReservationException e) {
                        System.out.println(e);
                    }
                    break;
                case 4:
                    showRooms();
                    break;
                case 5:
                    RoomUtility.roomCount(rooms);
                    break;
                case 6:
                    RoomUtility.roomSearch(rooms);
                    break;
                case 7:
                    break;
                default:
                    System.out.println("Invalid option.");
            }
        }

    }

    /**
     * Loop through the rooms to check if the room already exists.
     *
     * @param roomNumber
     * @return the index of the room number
     */
    public static int getRoomNumberIfExists(int roomNumber) {
        int index = -1;
        for (int i = 0; i < rooms.size(); i++) {
            if (rooms.get(i).getRoomNumber() == roomNumber) {
                index = i;
            }
        }
        return index;
    }

    /**
     * This method will allow the user to add a new room to the collection of
     * rooms.
     *
     * ###########Updated###############
     *
     * @author Jamison Peconi
     * @since 04/17/2016 Assignment 9 - Exception Handling
     *
     * Added exception handling to any input to make sure the program doesn't
     * crash if the user enters bad inputs.
     */
    public static void addRoom() {

        //***********************************************************
        //Ask which room number the user wants to add
        //***********************************************************
        Room room = null;
        Scanner input = new Scanner(System.in);
        // Catch any exceptions here that might be thrown if user enters invalid input
        //######### Update ############
        // Use the method in the utility class that will validate a number and return in
        // Exception handling done inside that method
        roomNum = Utility.validateNumberInput("Enter room number");
        //***********************************************************
        //Check to see if the room already exists
        //***********************************************************
        int roomNumberIndex = getRoomNumberIfExists(roomNum);

        //If the room does not already exist.
        if (roomNumberIndex == ROOM_DOES_NOT_EXIST) {
            roomNumberIndex = rooms.size();
            boolean finished = false;
            do {
                System.out.print("What type of room is this?\n" + "1) Add Room\n"
                        + "2) Computer Lab\n"
                        + "3) Board Room\n"
                        + "4) Biology lab\n"
                        + "5) Fancy Board Room");
                String choice = input.nextLine();

                //***********************************************************
                //Based on the user input, create the correct type of room.  
                //***********************************************************
                switch (choice) {
                    case "1":
                        room = new Room(roomNum);
                        finished = true;
                        break;
                    case "2":
                        room = new ComputerRoom(roomNum);
                        finished = true;
                        break;
                    case "3":
                        room = new BoardRoom(roomNum);
                        finished = true;
                        break;
                    case "4":
                        room = new BiologyLab(roomNum);
                        finished = true;
                        break;
                    case "5":
                        room = new BoardRoomPro(roomNum);
                        finished = true;
                        break;
                    default:
                        System.out.println("Invalid option");

                }
            } while (!finished);

            //Set the details for the room
            //Note the correct method will be invoked based on which type of room was created above.
            room.getRoomDetailsFromUser();

            //Add the room to the collection of rooms.  Note that as long as an object 'is a' Room 
            //(all of the types of rooms above are rooms), then it can be added to the collection of 
            //rooms.
            rooms.add(room);

        } else {
            String choice = "";
            System.out.println("Room already exists. Do you want to continue? (Y/N)");
            choice = input.nextLine();

            //If the user wants to continue, invoke the method to change the value of attributes in 
            //the room
            if (choice.equalsIgnoreCase("y")) {
                rooms.get(roomNumberIndex).getRoomDetailsFromUser();
            }
        }
    }

    /**
     * This method will allow the user to reserve a room. ################
     * Updated #######################
     *
     * @throws exception.RoomExistException
     * @throws exception.RoomReservationException
     * @author Jamison Peconi
     * @since 04/18/2016
     *
     * Added exception handling to this method. Now if the user enters any
     * invalid input it will be handled. Also added the custom exceptions in the
     * event that the room is already booked or doesnt exist.
     */
    public static void reserveRoom() throws RoomExistException, RoomReservationException {
        // Use the method in the Utility class to validate integer input
        int roomNumber = Utility.validateNumberInput("Enter the room number you would like to book");
        //Check to see if the room exists.
        int roomNumberIndex = getRoomNumberIfExists(roomNumber);
        if (roomNumberIndex < 0) {
            throw (new RoomExistException());
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = rooms.get(roomNumberIndex);
            if (!room.isReserved()) {
                room.reserveThisRoom();
            } else {
                throw (new RoomReservationException());
            }
        }
    }

    /**
     * This method will allow the user to reserve a room. ################
     * Updated #######################
     *
     * @throws exception.RoomExistException
     * @throws exception.RoomReservationException
     * @author Jamison Peconi
     * @since 04/18/2016
     *
     * Added exception handling to this method. Now if the user enters any
     * invalid input it will be handled. Also added the custom exceptions in the
     * event that the room is already booked or doesn't exist.
     */
    public static void releaseRoom() throws RoomExistException, RoomReservationException {
        // Use the method in the utility class to verify the integer input
        int roomNumber = Utility.validateNumberInput("Enter the room number you would like to book");
        //Check if the room exists.  
        int roomNumberIndex = getRoomNumberIfExists(roomNumber);

        if (roomNumberIndex < 0) {
            throw (new RoomExistException());
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = rooms.get(roomNumberIndex);
            //If the room is reserved, allow them to release.
            if (room.isReserved()) {
                room.releaseThisRoom();
            } else {
                throw (new RoomReservationException("This room is not booked!"));
            }
        }
    }

    /**
     * Show the details for each room
     */
    public static void showRooms() {
        for (int i = 0; i < rooms.size(); i++) {
            System.out.println(rooms.get(i));
        }
    }
}
