
import java.util.Scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bjmaclean
 */
public class ComputerRoom extends Room {

    int numberOfComputers;

    public ComputerRoom(int roomNumber) {
        super(roomNumber);
    }

    /**
     * Get the details from the user about this class. This will invoke the
     * super method to get the base class attributes.
     *
     * ############################ Update ##################################
     *
     * @author Jamison Peconi
     * @since 04/19/2016 Assignment 8
     *
     * Added exceptional handling to verify user number input. Prevents the
     * program from crashing.
     */
    public void getRoomDetailsFromUser() {
        super.getRoomDetailsFromUser();
        numberOfComputers = utility.Utility.validateNumberInput("Enter number of computers: ");
    }

    public int getNumberOfComputer() {
        return numberOfComputers;
    }

    public void setNumberOfComputer(int numberOfComputer) {
        this.numberOfComputers = numberOfComputer;
    }

    public String toString() {
        return super.toString() + "\nNumber of Computers" + numberOfComputers;
    }
}
