
package exception;

/**
 *
 * @author jpeconi
 */
public class RoomExistException extends Exception {

    public RoomExistException() {
        super("This room doesn't exist");
    }

    public RoomExistException(String string) {
        super(string);
    }
    
    
    
    
    
}
