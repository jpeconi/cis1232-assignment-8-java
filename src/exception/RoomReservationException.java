package exception;

/**
 *
 * @author jpeconi
 */
public class RoomReservationException extends Exception {

    public RoomReservationException() {
        super("This room is already reserved!");
    }

    public RoomReservationException(String string) {
        super(string);
    }

    
}
