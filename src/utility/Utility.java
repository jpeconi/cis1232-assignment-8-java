package utility;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Jamison Peconi
 * @since 01/18/2016
 *
 * This class is the Utility class. This class contains tools used in other
 * classes.
 */
public class Utility {

    //Universal variables to be used in other Java Programs
    private static final Scanner INPUT = new Scanner(System.in);
    public static final String TAB = "\t";

    /**
     * @jpeconi @date 02/01/2016
     * @return
     *
     * This is the getter to access the scanner.
     */
    public static Scanner getInput() {
        return INPUT;
    }

    public static String promptString(String question) {
        System.out.print(question);
        return INPUT.nextLine();
    }

    /**
     * @author jpeconi
     * @date 02/01/2016
     * @param question
     * @return
     *
     * This method accepts the prompt to be asked of the user. It then stores
     * the integer value and returns it to the method call.
     */
    public static int promptInt(String question) {
        int temp;
        System.out.print(question);
        temp = INPUT.nextInt();
        INPUT.nextLine();
        return temp;
    }

    /**
     * @author Jamison Peconi
     * @date 02/01/2016
     * @param array
     *
     * This method displays a String array. Accepts the array to be displayed as
     * a parameter.
     */
    public static void showStringArray(String[] array) {

        for (String val : array) {
            System.out.println(val);
        }

    }

    /**
     * @author Jamison Peconi
     * @date 02/01/2016
     * @param array
     *
     * This method displays an integer array. It accepts the array to be
     * displayed.
     */
    public static void showIntArray(int[] array) {
        for (int val : array) {
            System.out.println(val);
        }
    }

    /**
     * @author Jamison Peconi
     * @date 02/01/2016
     * @param array
     * @return
     *
     * This method accepts an array, it then reverses the values and stores them
     * in a new array. This is for Integers.
     */
    public static int[] reverseIntArray(int[] array) {
        int[] reverseArray = new int[array.length];
        for (int x = 0; x < array.length; x++) {
            reverseArray[array.length - (x + 1)] = array[x];
        }
        return reverseArray;
    }

    /**
     * @author Jamison Peconi
     * @since 02/23/2016
     * @param min
     * @param max
     * @return
     *
     * This method generates a random number between a minimum and maximum value
     * passed to the method. ( I borrowed this code from the web ). Stack
     * Overflow
     */
    public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((1000 - 1) + 1) + min;

        return randomNum;
    }

    /**
     * @author Jamison Peconi
     * @since 03/02/2016
     * @param map
     *
     * This method displays both the key and value of a Hash Map.
     *
     */
    public static void displayHashMap(HashMap<Object, Object> map) {
        for (Map.Entry<Object, Object> currentEntry : map.entrySet()) {
            System.out.println(currentEntry.getKey() + "   " + currentEntry.getValue());
        }
    }

    /**
     * @author Jamison Peconi
     * @since 04/18/2016
     * @param prompt
     * @return
     *
     * This method will accept a prompt from the user and will continue to
     * prompt the user until they enter a valid input. If they enter any invalid
     * input, then there will be an exception thrown and caught by this method.
     * Checks to make sure it is a number above 0.
     */
    public static int validateNumberInput(String prompt) {
        int userChoice = 0;
        boolean isValid;

        do {
            try {
                isValid = true;
                System.out.println(prompt);
                userChoice = getInput().nextInt();
                // Validates to ensure no negative numbers are given
                if (userChoice < 0) {
                    System.out.println("Must be a positive number!");
                    isValid = false;
                }
            } catch (Exception e) {
                System.out.println("Invalid input");
                // Set flag false to continue the loop
                isValid = false;
            }
            // Burn the line to avoid the infinite loop
            getInput().nextLine();
        } while (!isValid);
        return userChoice;
    }

    /**
     * @author Jamison Peconi
     * @param min
     * @param max
     * @since 04/18/2016
     * @param prompt
     * @return
     *
     * This method will accept a prompt from the user and will continue to
     * prompt the user until they enter a valid input. If they enter any invalid
     * input, then there will be an exception thrown and caught by this method.
     * This method also contains an input range. This will provide the user with
     * the ability to require an answer between two numbers. It will also check
     * to ensure no negative numbers were given.
     */
    public static int validateNumberInput(String prompt, int min, int max) {
        int userChoice = 0;
        boolean isValid;

        do {
            try {
                isValid = true;
                System.out.println(prompt);
                userChoice = getInput().nextInt();
                // Let the user know of their mistake
                // Makes sure the user enters a number between the min given and the max
                if (userChoice < min || userChoice > max) {
                    System.out.println("Invalid! Between " + min + " and " + max + " please");
                    isValid = false;
                    // Validates to ensure no negative number is given
                } else if (userChoice < 0) {
                    System.out.println("Must be a positive number!");
                    isValid = false;
                }
            } catch (Exception e) {
                System.out.println("Invalid input");
                // Set flag false to continue the loop
                isValid = false;
            }
            // Burn the line to avoid the infinite loop
            getInput().nextLine();
        } while (!isValid || (userChoice < min || userChoice > max));
        return userChoice;
    }

}
